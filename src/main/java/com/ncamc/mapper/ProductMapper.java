package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ncamc.entity.Product;

public interface ProductMapper extends BaseMapper<Product> {
}