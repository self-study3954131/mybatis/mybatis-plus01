package com.ncamc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface UserMapper extends BaseMapper<User> {

    Map<String,Object> selectMapById(Long id);

    Page<User> selectPageVo(Page<User> page,@Param("age") String age);

    Integer select(@Param("year")String year,@Param("quarter")String quarter);
}