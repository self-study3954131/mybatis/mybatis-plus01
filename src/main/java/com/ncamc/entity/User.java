package com.ncamc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ncamc.enums.SexEnum;
import lombok.Data;

@Data
//设置实体类所对应的表名
@TableName("t_user")
public class User {

    //@TableId注解的value属性用于指定主键的字段
    //@TableId注解的type属性用于设置主键生成策略 确保数据库设置了ID自增，否则无效
    //@TableId(value = "id",type = IdType.AUTO)
    //将属性所对应的字段指定为主键
    @TableId
    private Long id;

    //指定属性所对应的字段名
    @TableField("user_name")
    private String name;

    private SexEnum sex;

    private Integer age;

    private String email;

    //逻辑删除注解  逻辑未删除值  逻辑删除值
    @TableLogic(value = "0",delval = "1")
    private Integer delCode;

}
