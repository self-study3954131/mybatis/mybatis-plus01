package com.ncamc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisPlusApplication01 {
    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication01.class,args);
    }
}