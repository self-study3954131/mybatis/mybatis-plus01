package com.ncamc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ncamc.entity.Product;

public interface ProductService extends IService<Product> {
}