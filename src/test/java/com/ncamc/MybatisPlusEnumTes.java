package com.ncamc;

import com.ncamc.entity.User;
import com.ncamc.enums.SexEnum;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

@SpringBootTest
public class MybatisPlusEnumTes {

    @Autowired
    private UserMapper userMapper;

    /**
     * 通过枚举添加用户性别
     */
    @Test
    public void test1(){
        User user = new User();
        user.setName("admin");
        user.setAge(22);
        user.setSex(SexEnum.MALE);
        userMapper.insert(user);
    }
}
