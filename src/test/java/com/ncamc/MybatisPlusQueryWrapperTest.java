package com.ncamc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MybatisPlusQueryWrapperTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 组装查询条件
     * 查询用户名包含a，年龄在20到30之间，邮箱信息不为null的用户信息
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 AND (user_name LIKE ? AND age BETWEEN ? AND ? AND email IS NOT NULL)
     */
    @Test
    public void test1(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("user_name","a")
                    .between("age",20,30)
                    .isNotNull("email");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * 组装排序条件
     * 查询用户信息，按照年龄的降序排序，若年龄相同，则按照id升序排序
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 ORDER BY age DESC,id ASC
     */
    @Test
    public void test2(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("age")
                    .orderByAsc("id");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * 组装删除条件
     * 删除邮箱地址为null的数据
     * sql: UPDATE t_user SET del_code=1 WHERE del_code=0 AND (email IS NULL)
     */
    @Test
    public void test3(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("email");
        int result = userMapper.delete(queryWrapper);
        System.out.println(result);
    }

    /**
     * 使用QueryWrapper实现修改功能
     * 将（年龄大于20并且用户名中包含有a）或邮箱为null的用户信息修改
     * sql: UPDATE t_user SET user_name=?, email=? WHERE del_code=0 AND (age > ? AND user_name LIKE ? OR email IS NULL)
     */
    @Test
    public void test4(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.gt("age",20)
                    .like("user_name","a")
                    .or()
                    .isNull("email");
        User user = new User();
        user.setName("小明");
        user.setEmail("test@atguigu.com");
        userMapper.update(user,queryWrapper);
    }

    /**
     * 条件的优先级
     * 将用户名中包含有a并且（年龄大于20或邮箱为null）的用户信息修改
     * lambda中的条件优先执行
     * sql: UPDATE t_user SET user_name=?, email=? WHERE del_code=0 AND (user_name LIKE ? AND (age > ? OR email IS NULL))
     */
    @Test
    public void test5(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("user_name","a")
                    .and(i-> i.gt("age",20)
                              .or()
                              .isNull("email"));
        User user = new User();
        user.setName("小红");
        user.setEmail("test@atguigu.com");
        userMapper.update(user,queryWrapper);
    }

    /**
     * 组装select字句
     * 查询用户的  用户名、年龄、邮箱信息
     * sql: SELECT user_name,age,email FROM t_user WHERE del_code=0
     */
    @Test
    public void test6(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("user_name","age","email");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * 组装子查询
     * 查询id小于等于100的用户信息
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 AND (id IN (select id from t_user where id<=100))
     */
    @Test
    public void test7(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql("id","select id from t_user where id<=100");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * 模拟开发中的组装条件
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 AND (age >= ? AND age <= ?)
     */
    @Test
    public void test8(){
        String username = "";
        Integer ageBegin = 20;
        Integer ageEnd = 30;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(username)){
            queryWrapper.like("user_name",username);
        }
        if (ageBegin != null){
            //ge大于等于
            queryWrapper.ge("age",ageBegin);
        }
        if (ageEnd != null){
            //le小于等于
            queryWrapper.le("age",ageEnd);
        }
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * condition组装条件
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 AND (age >= ? AND age <= ?)
     */
    @Test
    public void test9(){
        String username = "";
        Integer ageBegin = 20;
        Integer ageEnd = 30;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(username),"user_name",username)
                .ge(ageBegin != null,"age",ageBegin)
                .le(ageEnd != null,"age",ageEnd);
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

}