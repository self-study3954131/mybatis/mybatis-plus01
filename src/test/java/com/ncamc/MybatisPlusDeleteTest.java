package com.ncamc;

import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class MybatisPlusDeleteTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 根据ID逻辑删除
     * sql: UPDATE user SET del_code=1 WHERE id=? AND del_code=0
     */
    @Test
    public void test1(){
        System.out.println(userMapper.deleteById(1541676830802927618L));
    }

    /**
     * 根据map中所设置的条件删除用户
     * sql: UPDATE user SET del_code=1 WHERE name = ? AND age = ? AND del_code=0
     */
    @Test
    public void test2(){
        Map<String,Object> map = new HashMap<>();
        map.put("name","张三");
        map.put("age","23");
        System.out.println(userMapper.deleteByMap(map));
    }

    /**
     * 通过多个ID批量删除
     * sql: UPDATE user SET del_code=1 WHERE id IN ( ? , ? , ? ) AND del_code=0
     */
    @Test
    public void test3(){
        List<Long> list = Arrays.asList(1L, 2L, 3L);
        System.out.println(userMapper.deleteBatchIds(list));
    }

}