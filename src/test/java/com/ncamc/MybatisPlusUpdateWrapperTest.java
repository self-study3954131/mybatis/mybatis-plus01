package com.ncamc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MybatisPlusUpdateWrapperTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 将用户名中包含有a并且（年龄大于20或邮箱为null）的用户信息修改
     * sql: UPDATE t_user SET user_name=?,email=? WHERE del_code=0 AND (user_name LIKE ? AND (age > ? OR email IS NULL))
     */
    @Test
    public void test1() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.like("user_name", "a")
                .and(i -> i.gt("age", 20).or().isNull("email"));
        updateWrapper.set("user_name", "小黑").set("email", "abc@atguigu.com");
        userMapper.update(null, updateWrapper);
    }
}