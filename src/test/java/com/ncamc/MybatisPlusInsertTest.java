package com.ncamc;

import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MybatisPlusInsertTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 添加数据
     * sql: INSERT INTO user ( id, name, age, email ) VALUES ( ?, ?, ?, ? )
     */
    @Test
    public void test1() {
        User user = new User();
        user.setName("张三");
        user.setEmail("zhangsan@atguigu.com");
        user.setAge(23);
        int result = userMapper.insert(user);
        System.out.println("result"+result);
        System.out.println("id:"+user.getId());
    }
}