package com.ncamc;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MybatisPlusPluginsTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE del_code=0 LIMIT ?
     */
    @Test
    public void test(){
        String current_ = "1";
        String size_ = "3";
        Integer current = Integer.parseInt(current_);
        Integer size = Integer.parseInt(size_);
        Page<User> page = new Page<>(current,size);
        userMapper.selectPage(page, null);
        System.out.println(page.getRecords());//当前页数据
        System.out.println(page.getCurrent());//当前页码
        System.out.println(page.getSize());//总页数
        System.out.println(page.getPages());//当前页条目数
        System.out.println(page.getTotal());//总记录数
        System.out.println(page.hasNext());//有没有下一页数据
        System.out.println(page.hasPrevious());//有没有上一页
    }

    /**
     * sql: select id,user_name,age,email from t_user where age > ? LIMIT ?
     */
    @Test
    public void test3(){
        Page<User> page = new Page<>(1, 3);
        userMapper.selectPageVo(page,"20");
        System.out.println(page.getRecords());//当前页数据
        System.out.println(page.getCurrent());//当前页码
        System.out.println(page.getSize());//总页数
        System.out.println(page.getPages());//当前页条目数
        System.out.println(page.getTotal());//总记录数
        System.out.println(page.hasNext());//有没有下一页数据
        System.out.println(page.hasPrevious());//有没有上一页
    }
}
