package com.ncamc;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class MybatisPlusSelectTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询所有数据
     * sql: SELECT id,name,age,email,del_code FROM user WHERE del_code=0
     */
    @Test
    public void test1(){
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

    /**
     * 通过ID查询用户信息
     * slq: SELECT id,name,age,email,del_code FROM user WHERE id=? AND del_code=0
     */
    @Test
    public void test2(){
        User user = userMapper.selectById(1L);
        System.out.println(user);
    }

    /**
     * 根据多个ID查询用户信息
     * sql: SELECT id,name,age,email,del_code FROM user WHERE id IN ( ? , ? , ? ) AND del_code=0
     */
    @Test
    public void test3(){
        List<Long> list = Arrays.asList(1L, 2L, 3L);
        List<User> users = userMapper.selectBatchIds(list);
        users.forEach(System.out::println);
    }

    /**
     * 根据map集合中的条件查询用户信息
     * sql: SELECT id,name,age,email,del_code FROM user WHERE name = ? AND age = ? AND del_code=0
     * sql: SELECT id,user_name AS name,age,email,del_code FROM t_user WHERE user_name = ? AND age = ? AND del_code=0
     */
    @Test
    public void test4(){
        Map<String,Object> map = new HashMap<>();
        map.put("user_name","Jack");
        map.put("age",20);
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }

    /**
     * 自定义sql 根据ID查询封装成map集合返回
     * sql: select id,name,age,email from user where id=?
     */
    @Test
    public void test5(){
        Map<String, Object> map = userMapper.selectMapById(1L);
        System.out.println(map);
    }

    @Test
    public void test6(){
        String year = "2022";
        String quarter = "一";
        if (!StringUtils.isEmpty(year)){
            if(quarter.equals("一")){
                Integer count = userMapper.select(year,quarter);
                System.out.println(count);
            }
        }
    }
}