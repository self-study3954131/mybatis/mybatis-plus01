package com.ncamc;

import com.ncamc.entity.User;
import com.ncamc.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MybatisPlusServiceTest {

    @Autowired
    private UserService userService;

    /**
     * 查询总记录数
     * sql: SELECT COUNT( * ) FROM user WHERE del_code=0
     */
    @Test
    public void test1(){
        long count = userService.count();
        System.out.println("总记录数"+count);
    }

    /**
     * 批量添加
     * sql: INSERT INTO user ( id, name, age ) VALUES ( ?, ?, ? )
     */
    @Test
    public void test2(){
        List<User> list = new ArrayList<>();
        for (int i = 1; i <= 10 ; i++) {
            User user = new User();
            user.setName("ybc"+i);
            user.setAge(20+i);
            user.setEmail("ybc"+i+"@atguigu.com");
            list.add(user);
        }
        boolean b = userService.saveBatch(list);
        System.out.println(b);
    }
}