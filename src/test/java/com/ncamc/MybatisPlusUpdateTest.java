package com.ncamc;

import com.ncamc.entity.User;
import com.ncamc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MybatisPlusUpdateTest {

    @Autowired
    private UserMapper userMapper;

    /**
     * 修改用户信息
     * sql: UPDATE user SET name=?, email=? WHERE id=? AND del_code=0
     */
    @Test
    public void test1() {
        User user = new User();
        user.setId(6L);
        user.setName("李四");
        user.setEmail("lisi@atguigu.com");
        userMapper.updateById(user);
    }

}